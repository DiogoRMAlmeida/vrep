#include<vrep_ros_interface/vrep_interface.h>

namespace vrep_ros_interface
{
int Interface::getID(std::string name)
{
    boost::lock_guard<boost::mutex> guard(vrep_mtx);
    ROS_DEBUG("Getting ID %s", name.c_str());
    srv_getID.request.objectName = name;

    if (client_getID.call(srv_getID))
      {
          if(srv_getID.response.handle == -1)
          {
              ROS_ERROR("Invalid handle for entity: %s", name.c_str());
          }
          else
          {
              ROS_DEBUG("Handle: %s %d", name.c_str(), srv_getID.response.handle);
          }
          return srv_getID.response.handle;
      }
      else
      {
          ROS_ERROR("Failed to call service /vrep/simRosGetObjectHandle");
          return -1;
      }
}

geometry_msgs::Pose Interface::getPose(int id, int ref_id)
{
    srv_getPose.request.handle = id;
    srv_getPose.request.relativeToObjectHandle = ref_id;

    if(!client_getPose.call(srv_getPose) || srv_getPose.response.result < 0)
    {
        ROS_ERROR("Cannot Get Pose of ID: %d",id);
    }

    return srv_getPose.response.pose.pose;
}

void Interface::setPose(int id, int ref_id, geometry_msgs::Pose pose)
{
    srv_setPose.request.handle = id;
    srv_setPose.request.relativeToObjectHandle = ref_id;
    srv_setPose.request.pose = pose;

    if(!client_setPose.call(srv_setPose) || srv_setPose.response.result < 0 )
    {
        ROS_ERROR("Cannot Set Pose");
    }
}

void Interface::setSignal(std::string name, int value)
{
    boost::lock_guard<boost::mutex> guard(vrep_mtx);
    srv_signal.request.signalName = name;
    srv_signal.request.signalValue = value;
    client_signal.call(srv_signal);
}

void Interface::startSimulation(ros::NodeHandle n_){
    client_start = n_.serviceClient<vrep_common::simRosStartSimulation>("/vrep/simRosStartSimulation");
    client_start.call(srv_start);
}

void Interface::stopSimulation(){
    sleep(2.0);
    client_stop.call(srv_stop);
}

bool Interface::isAt(geometry_msgs::Pose pose1, geometry_msgs::Pose pose2, float pos_tol, float ori_tol)
{
    boost::lock_guard<boost::mutex> guard(vrep_mtx);
    bool pos_check, ori_check;
    KDL::Rotation ori1, ori2, diff;
    double alpha, beta, gamma;
    tf::quaternionMsgToKDL(pose1.orientation, ori1);
    tf::quaternionMsgToKDL(pose2.orientation, ori2);
    diff = ori1.Inverse()*ori2;

    diff.GetEulerZYX(alpha, beta, gamma);

    pos_check = (std::abs(pose1.position.x - pose2.position.x)< pos_tol && std::abs(pose1.position.y - pose2.position.y)< pos_tol && std::abs(pose1.position.z - pose2.position.z)< pos_tol);
    ori_check = (std::abs(alpha) < ori_tol && std::abs(beta) < ori_tol && std::abs(gamma) < ori_tol);

    return (pos_check && ori_check);
}

bool Interface::isAt(int id_to_verify, int id_at, float tol)
{
    boost::lock_guard<boost::mutex> guard(vrep_mtx);
    geometry_msgs::Pose diff_pose = getPose(id_at,id_to_verify);
    return (std::abs(diff_pose.position.x)< tol && std::abs(diff_pose.position.y)< tol && std::abs(diff_pose.position.z)< tol);
}

bool Interface::isAt(int id_to_verify, int id_at, float pos_tol, float ori_tol)
{
    boost::lock_guard<boost::mutex> guard(vrep_mtx);
    geometry_msgs::Pose diff_pose = getPose(id_at,id_to_verify);
    double alpha, beta, gamma;
    bool ori_check;
    KDL::Rotation diff_ori;
    tf::quaternionMsgToKDL(diff_pose.orientation, diff_ori);

    diff_ori.GetEulerZYX(alpha, beta, gamma);
    ori_check = (std::abs(alpha) < ori_tol && std::abs(beta) < ori_tol && std::abs(gamma) < ori_tol);

    ROS_INFO("Alpha: %.4f\nBeta: %.4f\nGamma: %.4f", alpha, beta, gamma);

    return (std::abs(diff_pose.position.x)< pos_tol && std::abs(diff_pose.position.y)< pos_tol && std::abs(diff_pose.position.z)< pos_tol && ori_check);
}

bool Interface::isAt2D(geometry_msgs::Pose pose1, geometry_msgs::Pose pose2, float tol)
{
    boost::lock_guard<boost::mutex> guard(vrep_mtx);
    return (std::abs(pose1.position.x - pose2.position.x)< tol && std::abs(pose1.position.y - pose2.position.y)< tol);
}

float Interface::getDistance(int id, int ref_id)
{
    geometry_msgs::Pose this_pose = getPose(id, ref_id);
    return sqrt((this_pose.position.x)*(this_pose.position.x) + (this_pose.position.y)*(this_pose.position.y) + (this_pose.position.z)*(this_pose.position.z));
}

float Interface::getDistance(geometry_msgs::Pose pose_1, geometry_msgs::Pose pose_2)
{
    return sqrt((pose_1.position.x - pose_2.position.x)*(pose_1.position.x - pose_2.position.x) + (pose_1.position.y - pose_2.position.y )*(pose_1.position.y - pose_2.position.y) + (pose_1.position.z - pose_2.position.z)*(pose_1.position.z - pose_2.position.z));
}

void Interface::init_ids(ros::NodeHandle n)
{
    client_getID = n.serviceClient<vrep_common::simRosGetObjectHandle>("/vrep/simRosGetObjectHandle");
    client_start = n.serviceClient<vrep_common::simRosStartSimulation>("/vrep/simRosStartSimulation");
    client_stop = n.serviceClient<vrep_common::simRosStopSimulation>("/vrep/simRosStopSimulation");
    client_getPose = n.serviceClient<vrep_common::simRosGetObjectPose>("/vrep/simRosGetObjectPose");
    client_setPose = n.serviceClient<vrep_common::simRosSetObjectPose>("/vrep/simRosSetObjectPose");
    client_signal = n.serviceClient<vrep_common::simRosSetIntegerSignal>("/vrep/simRosSetIntegerSignal");
    client_move = n.serviceClient<vrep_common::simRosSetObjectPosition>("/vrep/simRosSetObjectPosition");
    client_setParent = n.serviceClient<vrep_common::simRosSetObjectParent>("/vrep/simRosSetObjectParent");

}

void Interface::printPose (geometry_msgs::Pose pose)
{
    ROS_INFO("position.x: %.2f\nposition.y: %.2f\nposition.z: %.2f", pose.position.x, pose.position.y, pose.position.z);
}

bool Interface::setParent(int id, int parent_id)
{
    boost::lock_guard<boost::mutex> guard(vrep_mtx);
    srv_setParent.request.handle = id;
    srv_setParent.request.parentHandle = parent_id;
    srv_setParent.request.keepInPlace = 1;

    client_setParent.call(srv_setParent);
}
}
