#include<vrep_ros_interface/vrep_yumi_interface.h>

namespace vrep_ros_interface
{
    Yumi::Yumi() :
      left_gripper_object_(""),
      right_gripper_object_(""),
      chassis_assembled_with_(""),
      pwc_assembled_with_(""),
      keyboard_assembled_with_(""),
      screen_assembled_with_("")
    {
        nh_ = ros::NodeHandle("~");
        vrep_interface_.init_ids(nh_);
        if(vrep_interface_.getID("Yumi") < 0)
        {
            ROS_ERROR("Yumi does not seem to be initialized in the current VRep scene");
            left_gripper_handle_ = -1;
            right_gripper_handle_ = -1;
            yumi_handle_ = -1;
        }
        else
        {
            left_gripper_handle_ = vrep_interface_.getID("left_handle");
            right_gripper_handle_ = vrep_interface_.getID("right_handle");
            left_grasp_handle_ = vrep_interface_.getID("left_arm_tip");
            right_grasp_handle_ = vrep_interface_.getID("right_arm_tip");
            yumi_handle_ = vrep_interface_.getID("Yumi");
        }

        left_grasp_ = false;
        right_grasp_ = false;
    }

    int Yumi::pickEef(int side)
    {
        switch(side)
        {
            case LEFT:
                return left_gripper_handle_;
                break;
            case RIGHT:
                return right_gripper_handle_;
                break;
            default:
                ROS_ERROR("Tried to pick eef for invalid side: %d", side);
                return -1;
        }
    }

    int Yumi::pickGrasp(int side)
    {
        switch(side)
        {
            case LEFT:
                return left_grasp_handle_;
                break;
            case RIGHT:
                return right_grasp_handle_;
                break;
            default:
                ROS_ERROR("Tried to pick grasp for invalid side: %d", side);
                return -1;
        }
    }

    void Yumi::setGrasp(int side, std::string name)
    {
        switch(side)
        {
            case LEFT:
                left_grasp_ = true;
                left_gripper_object_ = name;
                ROS_INFO("Setting left eef grasp to %s", name.c_str());
                break;
            case RIGHT:
                right_grasp_ = true;
                right_gripper_object_ = name;
                ROS_INFO("Setting right eef grasp to %s", name.c_str());
                break;
            default:
                ROS_ERROR("Tried to set grasp for invalid side: %d", side);
        }
    }

    void Yumi::unSetGrasp(int side)
    {
        switch(side)
        {
            case LEFT:
                left_grasp_ = false;
                left_gripper_object_.clear();
                ROS_INFO("Unsetting left eef grasp");
                break;
            case RIGHT:
                right_grasp_ = false;
                right_gripper_object_.clear();
                ROS_INFO("Unsetting right eef grasp");
                break;
            default:
                ROS_ERROR("Tried to unset grasp for invalid side: %d", side);
        }
    }

    /*
        Returns the eef side that is grasping the named object
    */
    int Yumi::getObjectEef(std::string name)
    {
        ROS_INFO("Getting end effector that is grasping %s", name.c_str());

        if(name == left_gripper_object_)
        {
            return LEFT;
        }

        if(name == right_gripper_object_)
        {
            return RIGHT;
        }

        ROS_WARN("Given object name (%s) is not being grasped by any eef!", name.c_str());
        return -1;
    }

    /*
        Returns the eef side that is grasping the named object
    */
    std::string Yumi::getEefObject(int eef)
    {
        switch(eef)
        {
            case LEFT:
                return left_gripper_object_;
                break;
            case RIGHT:
                return right_gripper_object_;
                break;
            default:
                ROS_ERROR("Given wrong eef: %d", eef);
                return std::string("");
        }
    }

    /*
        Moves the end-effector such that it aligns with the objects _grasp frame.

        The approach line is determined by the grasp frame z vector.
        The approach direction is defined by the direction of the rigid surface,
        or by the direction facing away from the eef currently grasping the object.
    */
    bool Yumi::approachObject(int eef, std::string object_name)
    {
        int grasping_eef = getObjectEef(object_name);
        int direction_multiplier = 1; // dirty dirty dirty...
        geometry_msgs::Pose object_pose, support_pose, new_pose;
        KDL::Frame object_kdl, support_kdl;
        int object_grasp_handle = vrep_interface_.getID(object_name + "_grasp");
        int object_handle = vrep_interface_.getID(object_name);
        KDL::Vector x, y, z;

        ROS_INFO("Getting pose");
        ROS_INFO("APPROACH OBJECT CALLED FOR OBJECT %s", object_name.c_str());
        if(object_grasp_handle == -1)
        {
          ROS_WARN("Non-graspable object");
          object_pose = vrep_interface_.getPose(object_handle, yumi_handle_);
        }
        else
        {
          object_pose = vrep_interface_.getPose(object_grasp_handle, yumi_handle_);
        }

        if(grasping_eef == -1) // Object is not grasped
        {
            int table_handle = vrep_interface_.getID("customizableTable");
            support_pose = vrep_interface_.getPose(table_handle, yumi_handle_);
        }
        else
        {
            int grasp_handle = pickGrasp(grasping_eef);
            support_pose = vrep_interface_.getPose(grasp_handle, yumi_handle_);
        }

        tf::poseMsgToKDL(object_pose, object_kdl);
        tf::poseMsgToKDL(support_pose, support_kdl);

        if(direction_multiplier*KDL::dot(object_kdl.M.UnitZ(), support_kdl.M.UnitZ()) > 0)
        {
            z = -object_kdl.M.UnitZ();
            y = -object_kdl.M.UnitY();
        }
        else
        {
            z = object_kdl.M.UnitZ();
            y = object_kdl.M.UnitY();
        }
        x = object_kdl.M.UnitX();

        KDL::Rotation new_rot(x, y, z);
        KDL::Frame new_kdl(new_rot, object_kdl.p);

        tf::poseKDLToMsg(new_kdl, new_pose);

        return moveEefToPose(eef, new_pose);
    }

    bool Yumi::approachObject(int eef1, int eef2)
    {
        // geometry_msgs::Pose eef2_pose, new_pose;
        // KDL::Frame eef2_kdl, new_kdl;
        // int eef2_handle = pickGrasp(eef2);
        //
        // eef2_pose = vrep_interface_.getPose(eef2_handle, yumi_handle_);
        // tf::poseMsgToKDL(eef2_pose, eef2_kdl);
        // new_kdl = eef2_kdl;
        // new_kdl.M = eef2_kdl.M.Inverse();
        // tf::poseKDLToMsg(new_kdl, new_pose);

        if(!getEefObject(eef2).empty())
        {
          return approachObject(eef1, getEefObject(eef2));
        }
        else
        {
          return approachObject(eef2, getEefObject(eef1));

        }
    }

    /*
        Changes the eef target to the pose of the target handle
    */
    bool Yumi::moveEefToPose(int eef, std::string pose_handle_name)
    {
        ROS_INFO("Moving to %s...", pose_handle_name.c_str());
        int ref_handle = vrep_interface_.getID(pose_handle_name);
        geometry_msgs::Pose pose = vrep_interface_.getPose(ref_handle, yumi_handle_);

        return moveEefToPose(eef, pose);
    }

    /*
        Controls a pose approach
    */
    bool Yumi::moveEefToPose(int eef, geometry_msgs::Pose pose)
    {
        int eef_handle = pickEef(eef);
        int current_grasp_handle = pickGrasp(eef);
        float dummy_time = 0.0;
        ros::Rate r(2);

        geometry_msgs::Pose grasp_pose = vrep_interface_.getPose(current_grasp_handle, yumi_handle_);
        vrep_interface_.setPose(eef_handle, yumi_handle_, pose);

        move_done_ = false;
        while(!vrep_interface_.isAt(grasp_pose, pose, POS_TOL, ORI_TOL) && dummy_time < MOVE_TIME_LIM)
        {
            grasp_pose = vrep_interface_.getPose(current_grasp_handle, yumi_handle_);
            r.sleep();
            dummy_time += 0.5;
        }
        move_done_ = true;
        ROS_INFO("Done");
        return true;
    }

    /*
        Attaches object_name_grasp to the given eef
    */
    bool Yumi::pick(int eef, std::string object_name)
    {
        ROS_INFO("Trying to pick object %s with eef %d", object_name.c_str(), eef);
        int grasp_handle = pickGrasp(eef);
        int object_handle = vrep_interface_.getID(object_name);

        setGrasp(eef, object_name);
        vrep_interface_.setParent(object_handle, grasp_handle);
    }

    /*
        Sets the table as the parent of the given object at a predefined height.
        Orientation is set so that the object remains flat agains the surface,
        avoiding clipping through it.
    */
    bool Yumi::place(std::string object_name)
    {
        ROS_INFO("Placing object %s", object_name.c_str());
        int object_handle = vrep_interface_.getID(object_name);
        int table_handle = vrep_interface_.getID("customizableTable");
        int eef = getObjectEef(object_name);
        geometry_msgs::Pose object_pose, table_pose;
        KDL::Frame object_kdl, table_kdl;
        KDL::Vector x, y, z;
        char closest_vector;

        object_pose = vrep_interface_.getPose(object_handle, table_handle);
        table_pose = vrep_interface_.getPose(table_handle, table_handle);

        tf::poseMsgToKDL(object_pose, object_kdl);
        tf::poseMsgToKDL(table_pose, table_kdl);

        // ROS_INFO("Previous vectors");
        // displayVector(object_kdl.M.UnitX());
        // displayVector(object_kdl.M.UnitY());
        // displayVector(object_kdl.M.UnitZ());
        closest_vector = getClosestVector(object_kdl.M, table_kdl.M.UnitZ());
        switch(closest_vector)
        {
            case 'x':
                x.x(0);
                x.y(0);
                if(KDL::dot(object_kdl.M.UnitX(), table_kdl.M.UnitZ()) > 0)
                {
                    x.z(1);
                }
                else
                {
                    x.z(-1);
                }

                y = object_kdl.M.UnitY();
                y.z(0);
                y.Normalize();

                z = object_kdl.M.UnitZ();
                z.z(0);
                z.Normalize();

                break;
            case 'y':
                y.x(0);
                y.y(0);
                if(KDL::dot(object_kdl.M.UnitY(), table_kdl.M.UnitZ()) > 0)
                {
                    y.z(1);
                }
                else
                {
                    y.z(-1);
                }

                x = object_kdl.M.UnitY();
                x.z(0);
                x.Normalize();

                z = object_kdl.M.UnitZ();
                z.z(0);
                z.Normalize();
                break;
            case 'z':
                z.x(0);
                z.y(0);
                if(KDL::dot(object_kdl.M.UnitZ(), table_kdl.M.UnitZ()) > 0)
                {
                    z.z(1);
                }
                else
                {
                    z.z(-1);
                }

                x = object_kdl.M.UnitY();
                x.z(0);
                x.Normalize();

                y = object_kdl.M.UnitZ();
                y.z(0);
                y.Normalize();
                break;
            default:
                ROS_ERROR("getClosestVector returned an unexpected value: %c", closest_vector);
                return false;
        }

        KDL::Rotation new_rot(x, y, z);
        KDL::Frame new_kdl(new_rot, object_kdl.p);

        // ROS_INFO("New vectors");
        // displayVector(new_rot.UnitX());
        // displayVector(new_rot.UnitY());
        // displayVector(new_rot.UnitZ());

        tf::poseKDLToMsg(new_kdl, object_pose);
        object_pose.position.z = 0.08;

        vrep_interface_.setPose(object_handle, table_handle, object_pose);
        vrep_interface_.setParent(object_handle, table_handle);
        unSetGrasp(eef);
        return true;
    }

    /*
        Transfer the object in the given eef to the other eef
    */
    bool Yumi::transferObject(int eef)
    {
        int new_grasp_handle, new_eef, object_handle;
        std::string obj_name = getEefObject(eef);

        switch(eef)
        {
            case LEFT:
                new_eef = RIGHT;
                new_grasp_handle = pickGrasp(RIGHT);
                break;
            case RIGHT:
                new_eef = LEFT;
                new_grasp_handle = pickGrasp(LEFT);
                break;
            default:
                ROS_ERROR("Wrong eef side given: %d", eef);
                return false;
        }

        object_handle = vrep_interface_.getID(obj_name);
        vrep_interface_.setParent(object_handle, new_grasp_handle);

        unSetGrasp(eef);
        setGrasp(new_eef, obj_name);
        return true;
    }

    /*
        Prints the given vector
    */
    void Yumi::displayVector(KDL::Vector v)
    {
        ROS_INFO("(%.3f, %.3f, %.3f)", v.x(), v.y(), v.z());
    }

    /*
        Compares the unit vectors of the given rotation and returns the label
        of the vector that's closes (in the dot product sense) to the given
        base_vector.

        Returns 'x', 'y', or 'z' for respectively the UnitX, UnitY and UnitZ
        vectors of the given rotation
    */
    char Yumi::getClosestVector(KDL::Rotation rot_axes, KDL::Vector base_vector)
    {
        char rotation_axis;
        float dot_total = 0.0, dot_temp;

        dot_temp = std::abs(KDL::dot(rot_axes.UnitX(), base_vector));
        if (dot_temp > dot_total)
        {
            dot_total = dot_temp;
            rotation_axis = 'x';
        }

        dot_temp = KDL::dot(rot_axes.UnitY(), base_vector);
        if (dot_temp > dot_total)
        {
            dot_total = dot_temp;
            rotation_axis = 'y';
        }

        dot_temp = KDL::dot(rot_axes.UnitZ(), base_vector);
        if (dot_temp > dot_total)
        {
            dot_total = dot_temp;
            rotation_axis = 'z';
        }

        ROS_INFO("Closest vector is %c", rotation_axis);

        return rotation_axis;
    }

    /*
        Attaches two objects, by setting the child object as child of the
        parent object's _snap frame.

        The child object will assume the same assemble frame as its parent
    */
    bool Yumi::assembleObjects(std::string child_object, std::string parent_object)
    {
        ROS_INFO("Assembling object %s with object %s as parent", child_object.c_str(), parent_object.c_str());
        int child_handle = vrep_interface_.getID(child_object);
        int child_assemble_handle = vrep_interface_.getID(child_object + "_assemble");
        std::string memory_name;

        if(child_object == SCREEN_NAME)
            {
              memory_name = KEYBOARD_NAME;
            }
            else
            {
              if(child_object == KEYBOARD_NAME)
              {
                memory_name = PWC_NAME;
              }
              else
              {
                if(child_object == PWC_NAME)
                {
                  memory_name = CHASSIS_NAME;
                }
                else
                {
                  memory_name = parent_object;
                }
              }
            }


        int parent_handle = vrep_interface_.getID(memory_name + "_snap");
        int parent_assemble_handle = vrep_interface_.getID(memory_name + "_assemble");
        geometry_msgs::Pose snap_pose, assemble_pose;
        snap_pose.position.x = 0;
        snap_pose.position.y = 0;
        snap_pose.position.z = 0;
        snap_pose.orientation.x = 0;
        snap_pose.orientation.y = 0;
        snap_pose.orientation.z = 0;
        snap_pose.orientation.w = 1;

        setAssembled(child_object, parent_object);
        setAssembled(parent_object, child_object);

        vrep_interface_.setPose(child_handle, parent_handle, snap_pose);

        assemble_pose = vrep_interface_.getPose(parent_assemble_handle, parent_handle);
        vrep_interface_.setPose(child_assemble_handle, child_handle, assemble_pose);

        vrep_interface_.setParent(child_handle, parent_handle);
        unSetGrasp(getObjectEef(child_object));
    }

    bool Yumi::areObjectsAssembled(std::string object_name_1, std::string object_name_2)
    {
      if(object_name_1 == CHASSIS_NAME)
      {
        return chassis_assembled_with_ == object_name_2;
      }
      if(object_name_1 == KEYBOARD_NAME)
      {
        return keyboard_assembled_with_ == object_name_2;
      }
      if(object_name_1 == SCREEN_NAME)
      {
        return screen_assembled_with_ == object_name_2;
      }
      if(object_name_1 == PWC_NAME)
      {
        return pwc_assembled_with_ == object_name_2;
      }
      else
      {
        ROS_ERROR("Object_name_1 (%s) is invalid!!!", object_name_1.c_str());
        return false;
      }
    }

    void Yumi::setAssembled(std::string object_name, std::string assembled_name)
    {
      if(object_name == CHASSIS_NAME)
      {
        chassis_assembled_with_ = assembled_name;
        return;
      }
      if(object_name == KEYBOARD_NAME)
      {
        keyboard_assembled_with_ = assembled_name;
        return;
      }
      if(object_name == SCREEN_NAME)
      {
        screen_assembled_with_ = assembled_name;
        return;
      }
      if(object_name == PWC_NAME)
      {
        pwc_assembled_with_ = assembled_name;
        return;
      }
      else
      {
        ROS_ERROR("Object_name (%s) is invalid!!!", object_name.c_str());
      }
    }

    void Yumi::startSimulation()
    {
        vrep_interface_.startSimulation(nh_);
    }

    void Yumi::stopSimulation()
    {
        vrep_interface_.stopSimulation();
    }

    bool Yumi::isEefFree(int eef)
    {
        switch(eef)
        {
            case LEFT:
                return !left_grasp_;
                break;
            case RIGHT:
                return !right_grasp_;
                break;
            default:
                ROS_ERROR("Got invalid eef handle: %d", eef);
                return false;
                break;
        }
    }

    /*
        Indicates if target object is within reach of the eef
    */
    bool Yumi::canEefReachObject(int eef, std::string object_name)
    {
        int surface_handle = vrep_interface_.getID("customizableTable");
        int grasp_handle = vrep_interface_.getID(object_name + "_grasp");

        geometry_msgs::Pose grasp_pose = vrep_interface_.getPose(grasp_handle, surface_handle);
        KDL::Frame grasp_kdl;

        tf::poseMsgToKDL(grasp_pose, grasp_kdl);


        if(grasp_kdl.p.y() > 0 && eef == LEFT)
        {
            return true;
        }
        else
        {
            if(grasp_kdl.p.y() < 0 && eef == RIGHT)
            {
                return true;
            }
            else
            {
                ROS_WARN("Warning! Eff %d cannot pick object %s", eef, object_name.c_str());
                return false;
            }
        }
    }

    /*
        Indicates if eef has an assemblable object in its grasp

        An assemblable object has the assembly direction facing outwards
        the eef.
    */
    bool Yumi::isAssemblable(int eef)
    {
        ROS_INFO("Checking assemblable status for eef %d", eef);
        ROS_INFO("Eef is grasping object: %s", getEefObject(eef).c_str());
        int object_handle, grasp_handle;
        geometry_msgs::Pose object_pose, grasp_pose;
        KDL::Frame object_kdl, grasp_kdl;
        KDL::Vector object_z, grasp_z;

        grasp_handle = pickGrasp(eef);
        grasp_pose = vrep_interface_.getPose(grasp_handle, yumi_handle_);

        object_handle = vrep_interface_.getID(getEefObject(eef) + "_assemble");
        object_pose = vrep_interface_.getPose(object_handle, yumi_handle_);

        tf::poseMsgToKDL(grasp_pose, grasp_kdl);
        tf::poseMsgToKDL(object_pose, object_kdl);

        object_z = object_kdl.M.UnitZ();
        grasp_z = grasp_kdl.M.UnitZ();

        if(KDL::dot(object_z, grasp_z) > 0)
        {
            ROS_INFO("Eef %d has an assemblable object in its grasp", eef);
            return true;
        }

        ROS_ERROR("Eef %d does not have an assemblable object in its grasp", eef);
        return false;
    }

    /*
        Checks if end-effectors are close
    */
    bool Yumi::isClose(int eef1, int eef2)
    {
        int grasp_handle_1, grasp_handle_2;

        grasp_handle_1 = pickGrasp(eef1);
        grasp_handle_2 = pickGrasp(eef2);

        return checkClose(grasp_handle_1, grasp_handle_2);
    }

    /*
        Checks if end-effector is close to object
    */
    bool Yumi::isClose (int eef, std::string object_name)
    {
        int grasp_handle = pickGrasp(eef);
        int object_handle;

        if(object_name != SURFACE_NAME)
        {
          object_handle = vrep_interface_.getID(object_name + "_grasp");
        }
        else
        {
          object_handle = vrep_interface_.getID(object_name);
        }

        return checkClose(grasp_handle, object_handle);
    }

    /*
        Checks if objects are close
    */
    bool Yumi::isClose(std::string object_name_1, std::string object_name_2)
    {
        int object_handle_1 = vrep_interface_.getID(object_name_1);
        int object_handle_2 = vrep_interface_.getID(object_name_2);

        return checkClose(object_handle_1, object_handle_2);
    }

    /*
        Checks if object is being grasped by the end-effector
    */
    bool Yumi::isObjectInEef(int eef, std::string object_name)
    {
        switch(eef)
        {
            case LEFT:
                return object_name == left_gripper_object_;
                break;
            case RIGHT:
                return object_name == right_gripper_object_;
                break;
            default:
                ROS_ERROR("Given wrong end-effector: %d", eef);
                return false;
        }
    }

    /*
        Checks if entities provided by the given handles have an L2 distance
        in the position smaller than CLOSE_TOL
    */
    bool Yumi::checkClose(int handle_1, int handle_2)
    {
        if(handle_1 == -1 || handle_2 == -1)
        {
            ROS_ERROR("Check close with invalid handles");
            return false;
        }

        if (handle_1 == left_grasp_handle_ && handle_2 == right_grasp_handle_)
        {
          return vrep_interface_.isAt(handle_1, handle_2, CLOSE_TOL);
        }
        else
        {
          // if (move_done_)
          // {
          //   move_done_ = false;
          //   return true;
          // }
          ROS_INFO("Checking close!");
          // return vrep_interface_.isAt(handle_1, handle_2, CLOSE_TOL);
          return vrep_interface_.isAt(handle_1, handle_2, CLOSE_TOL, ORI_TOL);
        }
    }
}
