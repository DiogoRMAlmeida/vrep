#ifndef __VREPROSINTERFACE__
#define __VREPROSINTERFACE__

#include "ros/ros.h"
#include "vrep_common/simRosGetObjectHandle.h"
#include "vrep_common/simRosGetObjectPose.h"
#include "vrep_common/simRosSetObjectPose.h"
#include "vrep_common/simRosSetObjectPosition.h"
#include "vrep_common/simRosSetJointTargetVelocity.h"
#include "vrep_common/simRosSetObjectParent.h"
#include "vrep_common/simRosSetJointTargetPosition.h"
#include "vrep_common/simRosSetIntegerSignal.h"
#include "vrep_common/simRosStartSimulation.h"
#include "vrep_common/simRosStopSimulation.h"

#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Twist.h"
#include <math.h>       /* sqrt */
#include <iostream>
#include <string>
#include <tf_conversions/tf_kdl.h>
#include <stddef.h>
#include <boost/thread.hpp>


namespace vrep_ros_interface
{
class Interface
{
public:
    Interface(){}
    int getID(std::string name); //gets the ID of the object with the name "name". The name is availabe at the scene hierchy
    geometry_msgs::Pose getPose(int id, int ref_id);//get pose of the object with the id "id" wrt the object with the id "ref_id" the world has id -1 i.e. to have the absolute position you should call vrep_ros_common::getPose(the_id,-1);
    void setPose(int id, int ref_id, geometry_msgs::Pose pose);//sets the pose
    void setSignal(std::string name, int value);//set a vrep signal
    void startSimulation(ros::NodeHandle n_);//starts the simulation
    void stopSimulation();//stops the simulation
    float getDistance(int id, int ref_id);//get the euclidean distance between the object with id : id and ref_id
    float getDistance(geometry_msgs::Pose pose_1, geometry_msgs::Pose pose_2);//get the euclidean distance between psoe_1 and pose_2
    void init_ids(ros::NodeHandle n_);//initializes the indexes
    bool isAt(int id_to_verify, int id_at, float tol);//check if id_to_verify is at id_st
    bool isAt(int id_to_verify, int id_at, float pos_tol, float ori_tol);//check if id_to_verify is at id_st
    bool isAt(geometry_msgs::Pose pose1, geometry_msgs::Pose pose2, float pos_tol, float ori_tol);//check if pose1 is at pose2
    bool isAt2D(geometry_msgs::Pose pose1, geometry_msgs::Pose pose2, float tol);//same as above but disregarding z
    void printPose (geometry_msgs::Pose pose);//guess...
    bool setParent(int id, int parent_id);

private:
    vrep_common::simRosGetObjectPose srv_getPose;
    vrep_common::simRosSetObjectPose srv_setPose;
    vrep_common::simRosSetIntegerSignal srv_signal;
    vrep_common::simRosSetObjectPosition srv_move;
    vrep_common::simRosStartSimulation srv_start;
    vrep_common::simRosStopSimulation srv_stop;
    vrep_common::simRosGetObjectHandle srv_getID;
    vrep_common::simRosSetObjectParent srv_setParent;

    ros::ServiceClient client_setParent;
    ros::ServiceClient client_getPose;
    ros::ServiceClient client_setPose;
    ros::ServiceClient client_signal;
    ros::ServiceClient client_move;
    ros::ServiceClient client_start;
    ros::ServiceClient client_stop;
    ros::ServiceClient client_getID;
    boost::mutex vrep_mtx;
};
}
#endif // VREPROSCOMMON_H
