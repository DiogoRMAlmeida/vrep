#ifndef __VREPYUMIINTERFACE__
#define __VREPYUMIINTERFACE__

#include <ros/ros.h>
#include <vrep_ros_interface/vrep_interface.h>
#include <vrep_ros_interface/remoteApi/extApi.h>
#include <kdl_conversions/kdl_msg.h>
#include <boost/thread.hpp>

#define LEFT 1
#define RIGHT 2
#define POS_TOL 0.005
#define CLOSE_TOL 0.02
#define ORI_TOL 0.2
#define MOVE_TIME_LIM 10

const std::string CHASSIS_NAME = "Chassis";
const std::string PWC_NAME = "PWC";
const std::string KEYBOARD_NAME = "Keyboard";
const std::string SCREEN_NAME = "LCD_screen";
const std::string SURFACE_NAME = "surface_pose";


namespace vrep_ros_interface
{
class Yumi
{
public:
    Yumi();

    /* Util */
    void startSimulation();
    void stopSimulation();

    /* Actions */
    bool moveEefToPose(int eef, std::string pose_handle_name);
    bool moveEefToPose(int eef, geometry_msgs::Pose pose);
    bool approachObject(int eef, std::string object_name);
    bool approachObject(int eef1, int eef2);

    bool pick(int eef, std::string object_name);
    bool assembleObjects(std::string child_object, std::string parent_object);
    bool place(std::string object_name);
    bool transferObject(int eef);

    /* Conditions */
    bool isEefFree(int eef);
    bool isAssemblable(int eef);
    bool isObjectInEef(int eef, std::string object_name);
    bool canEefReachObject(int eef, std::string object_name);
    bool isClose(int eef1, int eef2);
    bool isClose(int eef, std::string object_name);
    bool isClose(std::string object_name_1, std::string object_name_2);
    bool areObjectsAssembled(std::string object_name_1, std::string object_name_2);
    std::string getEefObject(int eef);
    int getObjectEef(std::string name);

    boost::mutex yumi_mtx;

private:
    int left_gripper_handle_, right_gripper_handle_, yumi_handle_;
    int left_grasp_handle_, right_grasp_handle_;

    bool left_grasp_, right_grasp_;
    bool move_done_;
    std::string left_gripper_object_, right_gripper_object_;
    std::string chassis_assembled_with_, pwc_assembled_with_, keyboard_assembled_with_, screen_assembled_with_;


    Interface vrep_interface_;
    ros::NodeHandle nh_;

    int pickEef(int side);
    int pickGrasp(int side);
    void setGrasp(int side, std::string name);
    void unSetGrasp(int side);
    void setAssembled(std::string object_name, std::string assembled_name);
    bool checkClose(int handle_1, int handle_2);

    // KDL operations
    char getClosestVector(KDL::Rotation rot_axes, KDL::Vector base_vector);
    void displayVector(KDL::Vector v);

};
}
#endif
