cmake_minimum_required(VERSION 2.8.3)
project(vrep_playground)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
  vrep_common
  vrep_ros_interface
  tf_conversions
  tf
)

link_directories(${catkin_LIBRARY_DIRS})

include_directories(
  ${catkin_INCLUDE_DIRS}
  include
)

catkin_package(
   INCLUDE_DIRS include
   LIBRARIES ${PROJECT_NAME}
   CATKIN_DEPENDS roscpp std_msgs vrep_common tf_conversions tf vrep_ros_interface
)

add_executable(vrep_playground src/playground.cpp)
target_link_libraries(vrep_playground ${catkin_LIBRARIES})
