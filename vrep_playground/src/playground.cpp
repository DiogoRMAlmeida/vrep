#include "ros/ros.h"
#include <vrep_ros_interface/vrep_yumi_interface.h>
#include <ros/console.h>


int main(int argc, char ** argv)
{
  ros::init(argc, argv, "playground");
  ros::NodeHandle n;
  vrep_ros_interface::Yumi yumi;

  yumi.startSimulation();
  yumi.approachObject(RIGHT, "Chassis");
  yumi.pick(RIGHT, "Chassis");
  yumi.moveEefToPose(RIGHT, "cellphone_pose_1");
  yumi.approachObject(LEFT, "PWC");
  yumi.pick(LEFT, "PWC");
  yumi.moveEefToPose(LEFT, "cellphone_pose_2");
  yumi.isAssemblable(RIGHT);
  yumi.moveEefToPose(LEFT, "cellphone_pose_4");
  yumi.place("PWC");
  yumi.moveEefToPose(LEFT, "cellphone_pose_2");
  yumi.approachObject(LEFT, "Chassis");
  yumi.transferObject(RIGHT);
  yumi.moveEefToPose(LEFT, "cellphone_pose_2");
  yumi.approachObject(RIGHT, "PWC");
  yumi.pick(RIGHT, "PWC");
  yumi.moveEefToPose(RIGHT, "cellphone_pose_1");
  yumi.approachObject(RIGHT, "Chassis");
  yumi.isAssemblable(LEFT);
  yumi.isAssemblable(RIGHT);
  yumi.assembleObjects("PWC", "Chassis");
  yumi.moveEefToPose(RIGHT, "rest_pose_2");
  yumi.moveEefToPose(LEFT, "cellphone_pose_4");
  yumi.place("Chassis");
  yumi.moveEefToPose(LEFT, "cellphone_pose_2");
  yumi.approachObject(RIGHT, "Keyboard");
  yumi.pick(RIGHT, "Keyboard");
  yumi.moveEefToPose(RIGHT, "cellphone_pose_1");
  yumi.approachObject(LEFT, "LCD_screen");
  yumi.pick(LEFT, "LCD_screen");
  yumi.moveEefToPose(LEFT, "cellphone_pose_2");
  yumi.approachObject(LEFT, "Keyboard");
  yumi.isAssemblable(LEFT);
  yumi.isAssemblable(RIGHT);
  yumi.assembleObjects("LCD_screen", "Keyboard");
  yumi.moveEefToPose(LEFT, "cellphone_pose_2");
  yumi.moveEefToPose(RIGHT, "cellphone_pose_3");
  yumi.place("Keyboard");
  yumi.moveEefToPose(RIGHT, "cellphone_pose_1");
  yumi.approachObject(LEFT, "Chassis");
  yumi.pick(LEFT, "Chassis");
  yumi.moveEefToPose(LEFT, "cellphone_pose_2");
  yumi.approachObject(RIGHT, "Keyboard");
  yumi.pick(RIGHT, "Keyboard");
  yumi.moveEefToPose(RIGHT, "cellphone_pose_1");
  yumi.approachObject(RIGHT, "Chassis");
  yumi.isAssemblable(LEFT);
  yumi.isAssemblable(RIGHT);
  yumi.assembleObjects("Keyboard", "PWC");
  yumi.moveEefToPose(RIGHT, "cellphone_pose_1");
  yumi.moveEefToPose(LEFT, "cellphone_pose_4");
  yumi.place("Chassis");
  yumi.moveEefToPose(LEFT, "cellphone_pose_2");



  // yumi.approachObject(RIGHT, "Chassis");
  // yumi.pick(RIGHT, "Chassis");
  // yumi.moveEefToPose(RIGHT, "cellphone_pose_1");
  // yumi.approachObject(LEFT, "Chassis");
  // yumi.transferObject(RIGHT);
  // yumi.moveEefToPose(LEFT, "cellphone_pose_4");
  // yumi.place("Chassis");
  // yumi.moveEefToPose(LEFT, "cellphone_pose_2");
  // yumi.approachObject(RIGHT, "Chassis");
  // yumi.pick(RIGHT, "Chassis");
  // yumi.moveEefToPose(RIGHT, "cellphone_pose_1");

  sleep(5.0);
  yumi.stopSimulation();

  return 0;
}
